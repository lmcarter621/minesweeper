from __future__ import print_function
from random import randrange


## o - open square, f - flagged square, x - bomb, # - number of adjacent mines

# this class should really be a singleton
class Board:
    board = []

    def __init__(self, rows, columns, number_of_mines=0):
        # initialize board dimensions and optional number of mines
        self.rows = rows
        self.columns = columns

        for r in range(rows):
            row = []
            for c in range(columns):
                row.append(Cell(r, c, None))
            self.board.append(row)

        self.number_of_mines = randrange(rows * columns) if number_of_mines is 0 else number_of_mines

    def position_mines(self):
        # randomly add the mines to board
        mine_count = 0
        while mine_count < self.number_of_mines:
            row = randrange(self.rows)
            col = randrange(self.columns)

            if 'x' not in self.board[row][col].value_stack:
                self.board[row][col].value_stack.append('x')
                mine_count+=1

    def select_square(self, x, y):
        if self.board[x][y].value_stack[0] is 'x':
            self.display_board()
            return
        else:
            self.open_square(x, y, self.count_adjacent_mines(x,y))

    def flag_square(self, row, column):
        if self.value_stack is not None or 'f' in self.value_stack:
            self.value_stack.append('f')
        elif 'f' in self.value_stack:
            self.value_stack.remove('f')

    def open_square(self, x, y, adjacent_mines):
        if adjacent_mines != 0:
            self.board[x][y].value_stack.insert(0, adjacent_mines)
            self.display_board()
        else:
            pass

    def count_adjacent_mines(self, row, column):
        adjacent_mine_count = 0
        if self.view_adjacents(row, column) is 'x':
            adjacent_mine_count+=1

        return adjacent_mine_count

    def find_nearest_mine(self, row, column):
        #find the nearest mine to a given coordinate
        pass

    def view_adjacents(self, row, column):
        # adjacent = [(current.x, current.y+1), (current.x-1, current.y+1), (current.x-1, current.y), (current.x-1, current.y-1),
#               (current.x, current.y-1), (current.x+1, current.y-1), (current.x+1, y), (current.x+1, current.y+1)]
        for r in xrange(row-1, row+2):
            for c  in xrange(column-1, column+2):
                if r != row and c != column and r >= 0 and c >=0 and r< self.rows and c<self.columns:
                    yield self.board[r][c]

    def display_board(self):
        for row in xrange(self.rows):
            for col in xrange(self.columns):
                if not self.board[row][col].value_stack:
                    print(None, end='\t')
                else:
                    print(self.board[row][col].value_stack[0], end='\t')
            print()


class Cell:

    def __init__(self, x, y, value=None):
        self.x = x
        self.y = y
        if value is not None:
            self.value_stack.append(value)
        else:
            self.value_stack = []


if __name__ == '__main__':
    minesweeper_board = Board(3, 5)
    minesweeper_board.position_mines()
    Board.display_board()